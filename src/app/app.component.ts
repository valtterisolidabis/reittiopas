import { Component } from '@angular/core';
import { Map } from './map/map';
import {NeverReturningGraphTraverser} from './route/traverser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor() {
    const startStop = Map.getBusStop('A');
    const endStop = Map.getBusStop('N');
    const traverser = new NeverReturningGraphTraverser();
    const allRoutes = traverser.getRoutes(startStop, endStop);
    const allDurations = allRoutes.map(transitions => transitions.map(transition => transition.getDuration()).reduce((acc, cum) => acc + cum));
    const routesSortedByTime = [...allRoutes].sort((routeA, routeB) => {
      const indexOfA = allRoutes.indexOf(routeA);
      const indexOfB = allRoutes.indexOf(routeB);
      return allDurations[indexOfA] - allDurations[indexOfB];
    });
    console.log(routesSortedByTime);
  }
}

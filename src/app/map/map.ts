import { reitit } from './reittiopas.json';

export class BusStop {
  private transitions: Transition[] = [];
  constructor(public id: string) {
  }

  public addTransition(transition: Transition) {
    this.transitions.push(transition);
  }

  getTransitions() {
    return this.transitions;
  }
}

export class Transition {

  constructor(
    private busLine: BusLine,
    private target: BusStop,
    private duration: number) {
  }

  public getTarget(): BusStop {
    return this.target;

  }

  public getBusLine(): BusLine {
    return this.busLine;
  }

  public getDuration(): number {
    return this.duration;
  }
}

class BusLine {
  constructor(private name: string, private busStops: BusStop[]) {
  }
}

export class Map {

  private static model = (() => {
    type TransitionType = { [index: string]: { [index: string]: number } };
    type ModelType = {
      transitions: TransitionType,
      busStops: {[id: string]: BusStop}
    };
    // tslint:disable-next-line:no-shadowed-variable
    const model: ModelType = {
      transitions: {},
      busStops: {}
    };
    // Create fast accessable model for transitions from node to another
    reitit.tiet.forEach(tie => {
      if (!model.transitions[tie.mista]) { model.transitions[tie.mista] = {}; }
      if (!model.transitions[tie.mista][tie.mihin]) { model.transitions[tie.mista][tie.mihin] = tie.kesto; }
      if (!model.transitions[tie.mihin]) { model.transitions[tie.mihin] = {}; }
      if (!model.transitions[tie.mihin][tie.mista]) { model.transitions[tie.mihin][tie.mista] = tie.kesto; }
      return true;
    });

    reitit.pysakit.forEach(pysakkiId => {
      model.busStops[pysakkiId] = new BusStop(pysakkiId);
    });

    [ 'keltainen', 'punainen', 'vihreä', 'sininen' ].forEach(color => {
      const busLine = new BusLine(color, reitit.linjastot[color].map(stopId => model.busStops[stopId]));
      const transtions: Transition[] = [];
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < reitit.linjastot[color].length; i++) {
        const from = reitit.linjastot[color][i];
        const to = reitit.linjastot[color][i + 1];
        if (from && to) {
          model.busStops[from].addTransition(new Transition(busLine, model.busStops[to], model.transitions[from][to]));
          model.busStops[to].addTransition(new Transition(busLine, model.busStops[from], model.transitions[to][from]));
        }
      }
    });
    return model;
  })();

  public static getBusStop(id: string) {
    return Map.model.busStops[id];
  }
}

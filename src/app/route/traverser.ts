import {BusStop, Transition} from '../map/map';

export class NeverReturningGraphTraverser {
  private alreadyVisited: BusStop[];
  private route: Transition[];
  private target: BusStop;
  private allRoutes: Transition[][];

  private enter(transition: Transition) {
    const busStop = transition.getTarget();
    if (this.alreadyVisited.indexOf(busStop) !== -1) {
      // Already visited this bus stop
      return;
    }
    this.alreadyVisited.push(busStop);
    this.route.push(transition);
    if (busStop === this.target) {
      // Arrived to target bus stop
      this.allRoutes.push([...this.route]);
    } else {
      // Continue traversing
      const transitions = busStop.getTransitions();
      transitions.forEach(transitionEntry => { this.enter(transitionEntry); });
    }
    this.alreadyVisited.pop();
    this.route.pop();
  }

  getRoutes(from: BusStop, to: BusStop): Transition[][] {
    this.alreadyVisited = [from];
    this.route = [];
    this.allRoutes = [];
    this.target = to;

    from.getTransitions().forEach(transition => {this.enter(transition); });
    return this.allRoutes;
  }
}
